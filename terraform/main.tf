# configured aws provider with proper credentials
provider "aws" {
  region = "us-east-1"
}


# create default vpc if one does not exit
resource "aws_default_vpc" "default_vpc" {

  tags = {
    Name = "default vpc"
  }
}


# use data source to get all avalablility zones in region
data "aws_availability_zones" "available_zones" {}


# create a default subnet in the first az if one does not exit
resource "aws_default_subnet" "subnet_az1" {
  availability_zone = data.aws_availability_zones.available_zones.names[0]
}

# create a default subnet in the second az if one does not exit
resource "aws_default_subnet" "subnet_az2" {
  availability_zone = data.aws_availability_zones.available_zones.names[1]
}

# create security group for the web server
resource "aws_security_group" "webserver_security_group" {
  name        = "webserver security group"
  description = "enable http access on port 80"
  vpc_id      = aws_default_vpc.default_vpc.id

  ingress {
    description = "http access"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "webserver security group"
  }
}

# create security group for the database
resource "aws_security_group" "database_security_group" {
  name        = "database security group"
  description = "enable mysql/aurora access on port 3306"
  vpc_id      = aws_default_vpc.default_vpc.id

  ingress {
    description     = "mysql/aurora access"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.webserver_security_group.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "database security group"
  }
}


# create the subnet group for the rds instance
resource "aws_db_subnet_group" "database_subnet_group" {
  name        = "database-subnet"
  subnet_ids  = [aws_default_subnet.subnet_az1.id, aws_default_subnet.subnet_az2.id]
  description = "subnet for database instance"

  tags = {
    Name = "database-subnet"
  }
}


# create the rds instance
resource "aws_db_instance" "db_instance" {
  engine                 = "mysql"
  engine_version         = "8.0.31"
  multi_az               = false
  identifier             = "dev-rds-instance"
  username               = "oscar"
  password               = "Admin123"
  instance_class         = "db.t2.micro"
  allocated_storage      = 200
  db_subnet_group_name   = aws_db_subnet_group.database_subnet_group.name
  vpc_security_group_ids = [aws_security_group.database_security_group.id]
  availability_zone      = data.aws_availability_zones.available_zones.names[0]
  db_name                = "applicationdb"
  skip_final_snapshot    = true
}


resource "aws_cloudwatch_dashboard" "main" {
  dashboard_name = "oscar-test-ACDC-dashboard"

  dashboard_body = jsonencode({
    widgets = [
      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "CPUUtilization",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Instance Level Resource Utilization CPU & Memory"
        }
      },
      {
        type   = "text"
        x      = 0
        y      = 7
        width  = 3
        height = 3

        properties = {
          markdown = "Instance Level CPU Utilization"
        }
      },

      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "FreeableMemory",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Freeable Memory"
        }
      },


      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "ReadLatency",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Read Latency"
        }

      },

      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "WriteLatency",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Write Latency"
        }
      },


      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "ReadIOPS",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Read IOPS"
        }

      },

      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "WriteIOPS",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Write IOPS"
        }

      },

      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "ACUUtilization",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "ACU Utilization"
        }

      },


      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "CommitLatency",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Commit Latency"
        }

      },


      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "DatabaseConnections",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Database Connections"
        }

      },

      {
        type   = "metric"
        x      = 0
        y      = 0
        width  = 12
        height = 6

        properties = {
          metrics = [
            [
              "AWS/RDS",
              "DeadLocks",
              "InstanceId",
              "dev-rds-instance"
            ]
          ]
          period = 300
          stat   = "Average"
          region = "us-east-1"
          title  = "Deadlocks"
        }

      }
    ]
  })
}


resource "aws_cloudwatch_metric_alarm" "ec2-cpu-alarm" {
  alarm_name                = "terraform-ec2-cpu-alarm1"
  comparison_operator       = "GreaterThanThreshold"
  evaluation_periods        = 2
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/RDS"
  period                    = 120
  statistic                 = "Average"
  threshold                 = 30
  alarm_description         = "This metric monitors ec2 cpu utilization reaches 80%"
  insufficient_data_actions = []
}

