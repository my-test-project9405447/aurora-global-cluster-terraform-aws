data "archive_file" "zip_python_file" {
  type = "zip"
  source_file = "./performance-insights.py"
  output_path = "./performance-insights.zip"
}


# Lambda Function
resource "aws_lambda_function" "performance_insights_counter_metrics" {
  function_name = "PerformanceInsightsCounterMetrics"
  runtime       = "provided.al2"  # Use a higher version runtime

  handler = "performance-insights.lambda_handler"
  filename = data.archive_file.zip_python_file.output_path  # Replace with the path to your Lambda function code

  role = aws_iam_role.performance_insights_counter_metrics_role.arn
  timeout = 60  # 1 minute timeout

    environment {
    variables = {
      INSTANCE_ID = "db-RAN5LNP2CPPAYDA7BH5N35LZEM"
    }
  }

  # Add any other configurations as needed
}

# IAM Role
resource "aws_iam_role" "performance_insights_counter_metrics_role" {
  name = "PerformanceInsightsCounterMetricsRole"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "pi_read_only_access" { 
  name       = "pi-read-only-access"
  policy_arn = "arn:aws:iam::aws:policy/AmazonRDSReadOnlyAccess"
  roles      = [aws_iam_role.performance_insights_counter_metrics_role.name]
}

resource "aws_iam_policy_attachment" "cloudwatch_full_access" {
  name       = "cloudwatch_full_access"
  policy_arn = "arn:aws:iam::aws:policy/CloudWatchFullAccess"
  roles      = [aws_iam_role.performance_insights_counter_metrics_role.name]
}

# Event Bridge Rule
resource "aws_cloudwatch_event_rule" "picountermetrics_5m" {
  name        = "PICounterMetrics5M"
  description = "Event rule for Performance Insights Counter Metrics"
  schedule_expression = "rate(5 minutes)"
}

resource "aws_cloudwatch_event_target" "lambda_target" {
  rule      = aws_cloudwatch_event_rule.picountermetrics_5m.name
  arn       = aws_lambda_function.performance_insights_counter_metrics.arn
}



/*data "http" "performance_insights_zip" {
  url = "https://gitlab.com/my-test-project9405447/aurora-global-cluster-terraform-aws/-/raw/master/performance-insights.py"
}

resource "null_resource" "download_file" {
  triggers = {
    always_run = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "curl -o ./path/to/local/directory/performance-insights.py ${data.http.performance_insights_zip.url}"
  }
}
*/
